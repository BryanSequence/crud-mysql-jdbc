package com.bryanjur.webservice.rest.services;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import javax.inject.Named;

import com.bryanjur.webservice.rest.dao.DBConnection;
import com.bryanjur.webservice.rest.data.PersonInfo;

@Named
public class GetPersonInfoResImpl {
	
	private int id;
	private String lastName;
	private String firstName;
	
	public void setId(int id) {
		this.id = id;
	}
	
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	
	public String addPersonInfo(Connection connection) throws Exception {
		PreparedStatement ps = connection.prepareStatement("INSERT INTO personal_db.person_info " + 
				"(lastname, firstname) VALUES (?, ?)");
		
		ps.setString(1,lastName);
		ps.setString(2, firstName);
		
		int count = ps.executeUpdate();
		
		if (count > 0) 
			return  "Success";
		else
			return "False";
	}
	
	public ArrayList<PersonInfo> getUsers(Connection connection) throws Exception {
		
		ArrayList<PersonInfo> user = new ArrayList<PersonInfo>();
		
		try {
			PreparedStatement ps = 
					connection.prepareStatement("SELECT id, lastname, firstname FROM person_info WHERE id =" + id);
			ResultSet rs = ps.executeQuery();
			
			while(rs.next())
			{
				PersonInfo p = new PersonInfo();
				p.setId(rs.getInt("id"));
				p.setLastName(rs.getString("lastname"));
				p.setFirstName(rs.getString("firstname"));
				user.add(p);
			}
		} catch(Exception e) {
			throw e;
		}		
		
		return user;
	}
	
	public String addPerson() {
		String res = "";
		try {
			DBConnection dbObj = new DBConnection();
			Connection con = dbObj.getConnection();
			res = addPersonInfo(con);
		} catch (Exception e) {
//			throws e;
		}
		return res;
	}
	
	public ArrayList<PersonInfo> GetUsersInfo() throws Exception {
		ArrayList<PersonInfo> personInfo = null;
		
		try {
			DBConnection dbObj = new DBConnection();
			Connection con = dbObj.getConnection();
			personInfo = getUsers(con);
		} catch (Exception e) {
			throw e;
		}
		
		return personInfo;
	}
	
}
