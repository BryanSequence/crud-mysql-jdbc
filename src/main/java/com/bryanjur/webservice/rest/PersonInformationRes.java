package com.bryanjur.webservice.rest;

import java.util.ArrayList;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.bryanjur.webservice.rest.data.PersonInfo;
import com.bryanjur.webservice.rest.services.GetPersonInfoResImpl;
import com.google.gson.Gson;

@Path("/person")
public class PersonInformationRes {
	
	GetPersonInfoResImpl getPersonInfoResImpl = new GetPersonInfoResImpl();
	
	@GET
	@Path("/info/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public String getPersonInfo(@PathParam("id") int id) {
		
		String personInfoResponse = "";
		ArrayList<PersonInfo> personInfo = null;
		
		getPersonInfoResImpl.setId(id);
		
		try {
			personInfo = getPersonInfoResImpl.GetUsersInfo();

			Gson gson = new Gson();
			personInfoResponse = gson.toJson(personInfo);
			
		} catch (Exception e) {}
		
		return personInfoResponse;
	}
	
	
	@POST
	@Path("/new")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response addNewPerson(PersonInfo personInfo) {
		
		System.out.println("flag1" + personInfo.toString());
		System.out.println("flag2" + personInfo.getFirstName());
		
		getPersonInfoResImpl.setFirstName(personInfo.getFirstName());
		getPersonInfoResImpl.setLastName(personInfo.getLastName());
		
		String result = "";
		
		result = getPersonInfoResImpl.addPerson();
		
		return Response.status(201).entity(result).build();
	}
 
}
